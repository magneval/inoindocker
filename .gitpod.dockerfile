# This file is a template, and might need editing before it works on your project.
FROM python:2.7

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#COPY requirements.txt /usr/src/app/
add https://downloads.arduino.cc/arduino-1.8.10-linux64.tar.xz  /usr/src/app

#RUN pip install --no-cache-dir -r requirements.txt
RUN cd   /usr/src/app/ 
RUN tar -axvf arduino-1.8.10-linux64.tar.xz
RUN pip install ino 

#COPY . /usr/src/app

#CMD ["python", "app.py"]
